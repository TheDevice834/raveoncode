@extends('base')

@section('body')
    <div class="container padding-top-25">
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger">{{ $error }}</div>
            @endforeach
        @endif
        Скоро...
    </div>
@endsection