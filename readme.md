<p align="center"><img src="https://raveoncode.ru/assets/img/favicon.png" width="150"></p>

## Установка
**Предварительно необходимо установить Composer и  NPM**

Для начала нужно создать файл окружения и настроить его:
```bash
cp .env.example .env
```

Далее нужно установить все необходимые зависимости Composer и NPM.

**Для Composer:**
```bash
composer install
```
**Для NPM:**
```bash
npm install
```
Теперь нужно выполнить миграции базы данных:
```bash
php artisan migrate
```
На этом всё. Сайт готов к использованию.

## Обновлени уже существующего репозитория
Выполните команду:
```bash
git pull
```


## Лицензия Laravel

Laravel Framework &#8212; это программное обеспечение с открытым исходным кодом лицензированное по [MIT license](http://opensource.org/licenses/MIT).
