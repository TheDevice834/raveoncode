<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Order;
use App\Ticket;
use App\Post;
use App\User;

class MainController extends Controller
{
    public function indexGet()
    {
        $posts = Post::orderBy('id', 'desc')->limit(3)->get();

        return view('main.index', [
            'title' => __('template.brand'),
            'posts' => $posts
        ]);
    }

    public function aboutGet()
    {
        $team = [];

        if($user = User::find(1)) $team[] = [
            'user' => $user,
            'post' => 'Сооснователь<br><b>Rave On Code</b>',
            'vk' => 'karprog'
        ];

        if($user = User::find(2)) $team[] = [
            'user' => $user,
            'post' => 'Сооснователь<br><b>Rave On Code</b>',
            'vk' => 'sniff1'
        ];

        return view('main.about', [
            'title' => 'О нас',
            'team' => $team
        ]);
    }

    public function contactGet()
    {
        return view('main.contact', [
            'title' => 'Связаться с нами'
        ]);
    }

    public function contactPost(Request $request)
    {
        $rules = [
            'name' => 'required|max:32',
            'email' => 'required|email|max:200',
            'body' => 'required'
        ];
        $messages = [
            'name.required' => 'Вы не указали имя',
            'name.max' => 'Максимальная длина имени - 32 символа',
            'email.required' => 'Вы не указали адрес электронной почты',
            'email.email' => 'Вы указали невалидный адрес электронной почты',
            'email.max' => 'Максимальная длина адреса электронной почты - 200 символов',
            'body.required' => 'Вы не указали текст заказа',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) return redirect()->route('contact')->withErrors($validator)->withInput();

        $order = new Order();
        $order->email = $request->input('email');
        $order->name = $request->input('name');
        $order->body = $request->input('body');

        if($order->save()) return redirect()->route('contact')->withErrors(['Заказ #'.$order->id.' успешно отправлен.']);
        else return redirect()->route('contact')->withErrors(['Что-то пошло не так. Попробуйте ещё раз.'])->withInput();
    }

    public function supportGet()
    {
        return view('main.support', [
            'title' => 'Поддержка'
        ]);
    }

    public function supportPost(Request $request)
    {
        $rules = [
            'title' => 'required|max:200',
            'body' => 'required'
        ];
        $messages = [
            'title.required' => 'Вы не указали тему',
            'title.max' => 'Максимальная длина темы - 200 символов',
            'body.required' => 'Вы не указали текст',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) return redirect()->route('support')->withErrors($validator)->withInput();

        $order = new Order();
        $order->title = $request->input('title');
        $order->body = $request->input('body');

        if($order->save()) return redirect()->route('support')->withErrors(['Тикет #'.$order->id.' успешно создан.']);
        else return redirect()->route('support')->withErrors(['Что-то пошло не так. Попробуйте ещё раз.'])->withInput();
    }
}
