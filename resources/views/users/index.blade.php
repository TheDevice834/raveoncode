@extends('base')

@section('body')
    <div class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Пользователи</h1>
            <p class="lead text-muted"><b>{{ $count }}</b> зарегистрировано</p>
        </div>
    </div>
    <div class="container">
        @if(count($users) >= 1)
            <div class="row">
                @foreach($users as $user)
                    <div class="col-lg-3">
                        <div class="card">
                            <img src="{{ $user->avatar() }}" alt="{{ $user->login }}" class="card-image" width="100%">
                            <div class="card-block">
                                <h3>{{ $user->login }}</h3>
                                <p>{{ $user->name }}</p>
                                <a href="{{ route('users.view', ['id' => $user->id]) }}" class="btn btn-outline-primary btn-block">Профиль</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <div class="alert alert-info">Нет материала для показа</div>
        @endif
        {{ $users->links('vendor.pagination') }}
    </div>
@endsection