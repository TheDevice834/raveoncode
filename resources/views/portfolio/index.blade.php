@extends('base')

@section('body')
    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Портфолио</h1>
            <p class="lead text-muted">Ниже предоставлен список работ, выполненных Rave On Code.</p>
            <p>
                <a href="{{ route('contact') }}" class="btn btn-primary"><i class="fa fa-cart-plus"></i> Заказать разработку</a>
            </p>
        </div>
    </section>
    <div class="album text-muted">
        <div class="container">
            @if(count($items) >= 1)
                <div class="row">
                    <div class="card">
                        <img src="https://content.makaan.com/17/1400382/275/3166311.jpeg?width=280&height=210" alt="Card image cap">
                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    </div>
                    <div class="card">
                        <img src="https://content.makaan.com/17/1400382/275/3166311.jpeg?width=280&height=210" alt="Card image cap">
                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    </div>
                    <div class="card">
                        <img src="https://content.makaan.com/17/1400382/275/3166311.jpeg?width=280&height=210" alt="Card image cap">
                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    </div>
                    <div class="card">
                        <img src="https://content.makaan.com/17/1400382/275/3166311.jpeg?width=280&height=210" alt="Card image cap">
                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    </div>
                    <div class="card">
                        <img src="https://content.makaan.com/17/1400382/275/3166311.jpeg?width=280&height=210" alt="Card image cap">
                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    </div>
                    <div class="card">
                        <img src="https://content.makaan.com/17/1400382/275/3166311.jpeg?width=280&height=210" alt="Card image cap">
                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    </div>
                    <div class="card">
                        <img src="https://content.makaan.com/17/1400382/275/3166311.jpeg?width=280&height=210" alt="Card image cap">
                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    </div>
                    <div class="card">
                        <img src="https://content.makaan.com/17/1400382/275/3166311.jpeg?width=280&height=210" alt="Card image cap">
                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    </div>
                    <div class="card">
                        <img src="https://content.makaan.com/17/1400382/275/3166311.jpeg?width=280&height=210" alt="Card image cap">
                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    </div>
                </div>
            @else
                <div class="alert alert-info">
                    Нет материала для показа
                </div>
            @endif
        </div>
    </div>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/portfolio.css') }}">
@endsection