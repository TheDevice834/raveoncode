@php $title = 'Страница не найдена' @endphp
@extends('errors')

@section('body')
    <div class="jumbotron text-center" style="background: #ed7b7b; color: #fff;">
        <div class="container">
            <h1>{{ $title }} <sup><span class="badge badge-danger">404</span></sup></h1>
            <p>Извините, но запрашиваемая Вами страница не была найдена.</p>
            <a href="{{ route('index') }}" class="btn btn-danger">Перейти на главную</a>
            <a href="{{ route('support') }}" class="btn btn-outline-danger">Сообщить об ошибке</a>
        </div>
    </div>
    <div class="container">
        <div class="card">
            <div class="card-block text-center">&copy; Copyright <a href="{{ route('index') }}">Rave On Code</a>, 2017. Все права защищены.</div>
        </div>
    </div>
@endsection