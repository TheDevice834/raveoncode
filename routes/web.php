<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// MainController
Route::get('/home', function(){
    return redirect()->route('index');
});
Route::get('/', ['as' => 'index', 'uses' => 'MainController@indexGet']);
Route::get('/about', ['as' => 'about', 'uses' => 'MainController@aboutGet']);
Route::get('/contact', ['as' => 'contact', 'uses' => 'MainController@contactGet']);
Route::post('/contact', ['as' => 'contact', 'uses' => 'MainController@contactPost']);
Route::get('/support', ['as' => 'support', 'uses' => 'MainController@supportGet'])->middleware('auth');
Route::post('/support', ['as' => 'support', 'uses' => 'MainController@supportPost'])->middleware('auth');

// AccountController
Route::get('/login', ['as' => 'login', 'uses' => 'AccountController@loginGet'])->middleware('guest');
Route::post('/login', ['as' => 'login', 'uses' => 'AccountController@loginPost'])->middleware('guest');
Route::get('/sign-up', ['as' => 'sign-up', 'uses' => 'AccountController@signUpGet'])->middleware('guest');
Route::post('/sign-up', ['as' => 'sign-up', 'uses' => 'AccountController@signUpPost'])->middleware('guest');
Route::get('/recovery', ['as' => 'recovery', 'uses' => 'AccountController@recoveryGet'])->middleware('guest');
Route::post('/recovery', ['as' => 'recovery', 'uses' => 'AccountController@recoveryPost'])->middleware('guest');
Route::get('/logout', ['as' => 'logout', 'uses' => 'AccountController@logoutGet'])->middleware('auth');

// PostsController
Route::get('/news', ['as' => 'news', 'uses' => 'PostsController@indexGet']);

// UsersController
Route::prefix('u')->group(function(){
    Route::get('/', ['as' => 'users', 'uses' => 'UsersController@indexGet']);
    Route::get('/{id}', ['as' => 'users.view', 'uses' => 'UsersController@viewGet'])->where(['id' => '[0-9]+']);
});

// PortfolioController
Route::prefix('portfolio')->group(function(){
    Route::get('/', ['as' => 'portfolio', 'uses' => 'PortfolioController@indexGet']);
});