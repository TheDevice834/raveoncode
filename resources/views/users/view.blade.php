@extends('base')

@section('body')
    <div class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">{{ $user->name }}</h1>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="card">
                    <img src="{{ $user->avatar() }}" alt="{{ $user->name }}" class="card-image" width="100%">
                    <div class="card-block">
                        {{ $user->name }}
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="card">
                    <div class="card-block">
                        <h3 class="card-title">Общая информация</h3>
                        <p class="card-text">
                            Номер аккаунта: <b>{{ $user->id }}</b><br>
                            Дата регистрации: <b>{{ date('m/d/Y', $user->created_at->timestamp) }}</b>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection