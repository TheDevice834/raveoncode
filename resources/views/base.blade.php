<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Rave On Code">
    <meta name="author" content="Rave On Code">
    <title>{{ $title }}</title>
    <link rel="icon" type="image/x-icon" href="{{ asset('assets/img/favicon.png') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/tether.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/base.css') }}">
    @yield('styles')
</head>
<body>
    <nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <h1 class="navbar-brand mb-0">Rave On Code <sup><span class="badge badge-danger">BETA</span></sup></h1>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item{{ (Route::currentRouteName() == 'index') ? ' active' : '' }}"><a class="nav-link" href="{{ route('index') }}"><i class="fa fa-home"></i> Главная</a></li>
                <li class="nav-item{{ (Route::currentRouteName() == 'news') ? ' active' : '' }}"><a class="nav-link" href="{{ route('news') }}"><i class="fa fa-archive"></i> Новости</a></li>
                <li class="nav-item{{ (Route::currentRouteName() == 'users') ? ' active' : '' }}"><a class="nav-link" href="{{ route('users') }}"><i class="fa fa-users"></i> Пользователи</a></li>
                <li class="nav-item{{ (Route::currentRouteName() == 'portfolio') ? ' active' : '' }}"><a class="nav-link" href="{{ route('portfolio') }}"><i class="fa fa-th-large"></i> Портфолио</a></li>
                <li class="nav-item{{ (Route::currentRouteName() == 'about') ? ' active' : '' }}"><a class="nav-link" href="{{ route('about') }}"><i class="fa fa-copyright"></i> О нас</a></li>
            </ul>
            @if(Auth::check())
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="{{ route('users.view', ['id' => Auth::user()->id]) }}" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-user"></i> {{ Auth::user()->login }}
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{ route('users.view', ['id' => Auth::user()->id]) }}"><i class="fa fa-id-card-o"></i> Профиль</a>
                        </div>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> Выйти</a></li>
                </ul>
            @else
                <div>
                    <a href="{{ route('login') }}" class="btn btn-success"><i class="fa fa-sign-in"></i> Вход</a>
                    <a href="{{ route('sign-up') }}" class="btn btn-secondary"><i class="fa fa-user-plus"></i> Регистрация</a>
                </div>
            @endif
        </div>
    </nav>
    @yield('body')
    <footer class="footer">
        <div class="container">
            <span class="text-muted">
                &copy; Copyright <a href="{{ route('index') }}">Rave On Code</a>, 2017. Все права защищены.
            </span>
        </div>
    </footer>

    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/tether.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/base.js') }}"></script>
    @yield('scripts')
</body>
</html>