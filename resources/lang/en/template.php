<?php

return [
    'brand' => 'Rave On Code',
    'home' => 'Home',
    'portfolio' => 'Portfolio',
    'about' => 'About',
    'profile' => 'Profile',
    'logout' => 'Logout',
    'login' => 'Login',
    'sign-up' => 'Sign up',
];