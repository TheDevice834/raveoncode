@extends('base')

@section('body')
    <div class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">О Rave On Code</h1>
            <p class="lead"><b>Rave On Code</b> &#8212; команда молодных разработчиков, которые решили объединить свои силы.<br>
                Мы выполняем свои работы маскимально качественно и максимально быстро.
                В основном, мы выполняем заказы на <b>разработку</b> или <b>поддержку сайтов</b>, но также мы пожем помочь с <b>настройкой виртуального сервера (VDS/VPS)</b>.</p>
            <p>
                <a href="{{ route('contact') }}" class="btn btn-primary btn-lg btn-block"><i class="fa fa-cart-plus"></i> Заказать разработку</a>
            </p>
        </div>
    </div>
    <div class="container">
        <h3>Состав команды на {{ date('d.m.Y') }}</h3>
        <hr>
        <div class="row">
            @if(!empty($team))
                @foreach($team as $item)
                    <div class="col-lg-3">
                        <div class="card">
                            <img src="{{ $item['user']->avatar() }}" class="card-image" width="100%">
                            <div class="card-block">
                                <h4 style="word-wrap: normal;">{{ $item['user']->name }}</h4>
                                <p class="card-text">
                                    {!! $item['post'] !!}
                                </p>
                                <a href="{{ route('users.view', ['id' => $item['user']->id]) }}" class="btn btn-secondary btn-block">
                                    Профиль
                                </a>
                                <a href="{{ url('https://vk.com/'.$item['vk']) }}" class="btn btn-outline-primary btn-block" target="_blank">
                                    <i class="fa fa-vk"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="alert alert-info">Нет материала для показа</div>
            @endif
        </div>
    </div>
@endsection