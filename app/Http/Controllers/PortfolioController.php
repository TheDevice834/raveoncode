<?php

namespace App\Http\Controllers;

use App\PortfolioItem;

class PortfolioController extends Controller
{
    public function indexGet()
    {
        $items = PortfolioItem::orderBy('id', 'DESC')->get();

        return view('portfolio.index', [
            'title' => 'Портфолио',
            'items' => $items
        ]);
    }
}