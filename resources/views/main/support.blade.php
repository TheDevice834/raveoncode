@extends('base')

@section('body')
    <div class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Поддержка</h1>
        </div>
    </div>
    <div class="container">
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger">{{ $error }}</div>
            @endforeach
        @endif
        <form class="form-horizontal" method="POST">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <h2>Форма отправки</h2>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 field-label-responsive">
                    <label>Тема</label>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-font"></i></div>
                            <input class="form-control" name="title" placeholder="Не работает страница" value="{{ old('title') }}" required autofocus>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 field-label-responsive">
                    <label for="password">Описание проблемы</label>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-comment-o"></i></div>
                            <textarea name="body" class="form-control" rows="6" placeholder="Описание проблемы"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <button  class="btn btn-success"><i class="fa fa-send"></i> Создать тикет</button>
                </div>
            </div>
        </form>
    </div>
@endsection