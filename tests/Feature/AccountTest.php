<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AccountTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetLogin()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }

    public function testGetSignUp()
    {
        $response = $this->get('/sign-up');

        $response->assertStatus(200);
    }

    public function testGetRecovery()
    {
        $response = $this->get('/recovery');

        $response->assertStatus(200);
    }

    public function testGetLogout()
    {
        $response = $this->get('/logout');

        $response->assertStatus(302);
    }
}
