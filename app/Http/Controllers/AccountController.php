<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\User;

class AccountController extends Controller
{
    public function loginGet()
    {
        return view('account.login', [
            'title' => 'Вход'
        ]);
    }

    public function loginPost(Request $request)
    {
        $rules = [
            'login' => 'required',
            'password' => 'required'
        ];
        $messages = [
            'login.required' => 'Вы не указали логин',
            'password.required' => 'Вы не указали пароль'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()) return redirect()->route('login')->withErrors($validator)->withInput();

        if(Auth::attempt(['login' => $request->input('login'), 'password' => $request->input('password')], true)) return redirect()->intended('/');
        else return redirect()->route('login')->withErrors(['Неверный адрес электронной почты или пароль'])->withInput();
    }

    public function signUpGet()
    {
        return view('account.sign-up', [
            'title' => 'Регистрация'
        ]);
    }

    public function signUpPost(Request $request)
    {
        $rules = [
            'email' => 'required|email|unique:users,email',
            'login' => 'required|unique:users,login|between:4,64|regex:/^([a-zA-Z0-9_-]+)$/',
            'name' => 'required|string',
            'password' => 'required|between:6,64',
            'password2' => 'same:password'
        ];
        $messages = [
            'email.required' => 'Вы не указали адрес электронной почты',
            'email.email' => 'Вы указали невалидный адрес электронной почты',
            'email.unique' => 'Данный адрес электронной почты уже занят',
            'login.required' => 'Вы не указали логин',
            'login.unique' => 'Данный логин уже занят',
            'login.between' => 'Логин должен быть не меньше 4-х символов и не больше 64-х символов',
            'login.regex' => 'Логин должен содержать только буквы английского алфавита (любого регистра), цифры, символы _ (нижнее подчеркивание) и - (минус)',
            'name.required' => 'Вы не указали имя',
            'name.string' => 'Имя должно быть строкой',
            'password.required' => 'Вы не указали пароль',
            'password.between' => 'Пароль должен быть не меньше 6-и символов и не больше 64-х символов',
            'password2.required' => 'Вы неправильно повторили пароль',
            'password2.same' => 'Вы неправильно повторили пароль',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()) return redirect()->route('sign-up')->withErrors($validator)->withInput();

        if($user = User::create([
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'login' => $request->input('login'),
            'name' => $request->input('name'),
        ])){
            Auth::login($user, true);
            return redirect()->route('index');
        }
        else return redirect()->route('sign-up')->withErrors(['Что-то пошло не так. Попробуйте ещё раз.'])->withInput();
    }

    public function recoveryGet()
    {
        return view('account.recovery', [
            'title' => 'Восстановление пароля'
        ]);
    }

    public function recoveryPost(Request $request)
    {
        return redirect()->route('recovery');
    }

    public function logoutGet()
    {
        Auth::logout();
        return redirect()->intended('/login');
    }
}
