@extends('base')

@section('body')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">Rave On Code</h1>
            <p>Мы предоставляем качественные и быстрые услуги по разработке и поддержке сайтов.</p>
            <p>
                <a href="{{ route('portfolio') }}" class="btn btn-primary btn-lg"><i class="fa fa-th-large"></i> Портфолио</a>
                <a href="{{ route('about') }}" class="btn btn-primary btn-lg"><i class="fa fa-copyright"></i> О нас</a>
            </p>
        </div>
    </div>
    <div class="container">
        <h1>Последние новости</h1>
        <hr>
        @if(count($posts) >= 1)
            <div class="row">
                @foreach($posts as $post)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-block">
                            <h2 class="card-title">{{ $post->title }}</h2>
                            <p class="card-text">{{ $post->text }}</p>
                            <a href="#" class="btn btn-secondary">Подробнее</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        @else
            <div class="alert alert-info">Новостей нет</div>
        @endif
    </div>
@endsection