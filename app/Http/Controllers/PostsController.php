<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostsController extends Controller
{
    public function indexGet()
    {
        $posts = Post::orderBy('id', 'DESC')->paginate(10);

        return view('posts.index', [
            'title' => 'Новости',
            'posts' => $posts
        ]);
    }
}
