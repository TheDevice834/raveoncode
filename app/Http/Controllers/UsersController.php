<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{
    public function indexGet()
    {
        $users = User::orderBy('id', 'DESC')->paginate(8);
        $count = User::count();

        return view('users.index', [
            'title' => 'Пользователи',
            'users' => $users,
            'count' => $count
        ]);
    }

    public function viewGet($id)
    {
        $user = User::where('id', $id)->first() or abort(404);

        return view('users.view', [
            'title' => $user->name,
            'user' => $user
        ]);
    }
}
