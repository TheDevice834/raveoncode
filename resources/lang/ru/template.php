<?php

return [
    'brand' => 'Rave On Code',
    'home' => 'Главная',
    'portfolio' => 'Портфолио',
    'about' => 'О нас',
    'profile' => 'Профиль',
    'logout' => 'Выйти',
    'login' => 'Войти',
    'sign-up' => 'Зарегистрироваться',
];